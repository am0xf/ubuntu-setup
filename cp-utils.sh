#!/bin/bash

user=$1

cp -r utils/ /home/$user/

ln -s /home/$user/utils/ion-login.sh /usr/bin/ion-login
chown $user:$user /usr/bin/ion-login
chmod 700 /usr/bin/ion-login

ln -s /home/$user/utils/sync-xt.sh /usr/bin/sync-xt
chown $user:$user /usr/bin/sync-xt
chmod 700 /usr/bin/sync-xt

