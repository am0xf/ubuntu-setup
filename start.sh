#!/bin/bash

user=$1

timedatectl set-ntp 1
timedatectl set-local-rtc 1
timedatectl set-timezone Asia/Kolkata
hwclock --systohc

./mkhome.sh $user

apt update

# basic packages for work
./install-pkgs.sh

# confs
./cp-confs.sh $user

# utils
./cp-utils.sh $user

apt upgrade -y
apt autoremove -y

