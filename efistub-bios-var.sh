#! /bin/bash

os=$3
esp=$1
root=$2
uuid=$(/sbin/blkid -s UUID -o value $root)

efibootmgr -v -d $esp -p 1 -c -L "$os" -l "\\EFI\\$os\\vmlinuz-linux" -u "root=UUID=$uuid rw initrd=\\EFI\\$os\\initramfs-linux.img"

