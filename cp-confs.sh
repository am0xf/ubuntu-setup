#!/bin/bash

user=$1

#cp confs.bkp/sudoers /etc/

cp confs.bkp/bash.bashrc /root/.bashrc
cp confs.bkp/tmux.conf /root/.tmux.conf
cp confs.bkp/vimrc /root/.vimrc

cp confs.bkp/bash.bashrc /home/$user/.bashrc
cp confs.bkp/tmux.conf /home/$user/.tmux.conf
cp confs.bkp/vimrc /home/$user/.vimrc

chown $user:$user /home/$user/{.bashrc,.tmux.conf,.vimrc}

