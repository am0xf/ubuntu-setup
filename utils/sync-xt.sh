#!/bin/bash

mount | grep xt-hd > /dev/null
[ $? -eq 1 ] && echo "Not mounted. Exiting." && exit 1

drs="docs lib pics vids"

for dir in $drs
do
	echo $dir
	[ -d /home/$USER/$dir ] && rsync -Prvh /home/$USER/$dir /mnt/xt-hd/
done

