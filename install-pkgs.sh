#!/bin/bash

apt install -y deluge qbittorrent fonts-inconsolata efibootmgr iw evince tmux vim gcc python3-pip vlc htop libreoffice firefox wget unzip zip nload iotop hddtemp thunderbird rsync p7zip gnome-tweak-tool gnome-tweaks

rm /usr/bin/vi
ln -s /usr/bin/vim /usr/bin/vi

